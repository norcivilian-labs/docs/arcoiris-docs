# arcoiris-docs

Documentation for [arcoiris](https://gitlab.com/norcivilian-labs/arcoiris), built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
