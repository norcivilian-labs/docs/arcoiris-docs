# Custom redistribution algorithm

By default, Arcoiris distributes shares of wealth in proportion to the amount of votes cast for each mission. Let's develop a new contract with an algorithm that gives each mission an equal share regardless of voting outcomes. At the end of this guide there will be a link to a template.

The algorithm needs to conform to the IRedistribution interface from the arcoiris repo.
```solidity
pragma solidity >=0.8.7 <0.9.0;

/// import {IRedistribution, Mission} from "@arcoiris/interfaces/IRedistribution.sol";
```

Now, scaffold the `redistribute` function, and leave the logic empty for now. The function takes a list of accounts, a list of votes for each account, and an amount of contributions made to the ceremony. It will return a list of Mission struct with a share of wealth distributed to each account.
```solidity
contract Even is IRedistribution {
    /// @notice Version of the contract, bumped on each deployment
    string public constant VERSION = "0.0.1";

    /// @notice Redistribute contributions among siblings evenly, ignoring the priorities
    /// @param siblings The list of ceremony members
    /// @param priorities Arbitrary number associated with each ceremony member
    /// @return missions Shares of wealth for each ceremony member
    function redistribute(
        address[] calldata siblings,
        uint256[] calldata priorities,
        uint256 amount
    ) external pure returns (Mission[] memory missions) {
        require(siblings.length == priorities.length, "E1");

        missions = new Mission[](siblings.length);

        // redistribution logic

        return missions;
    }
}
```

Now let's implement the logic. The wealth will distribute evenly, so each share will equal the total amount divided by the number of shares.
```solidity
        // redistribution logic
        uint256 sum;

        for (uint256 i = 0; i < siblings.length; i++) {
            uint256 share = amount / priorities.length;

            missions[i] = Mission(siblings[i], share);

            sum += share;
        }

        if (sum < amount) {
            missions[0] = Mission(siblings[0], amount - sum);
        }
```

Deploy this contract and specify its address during the next Poll creation.

> Don't forget to verify the contract at the explorer's page. See instructions on verification in the template repo.

See the [source code](https://gitlab.com/norcivilian-labs/arcoiris-algo-template) for this template.

