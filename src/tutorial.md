# Tutorial

Let's create a voting ceremony. First, open the application page at https://arcoiris.norcivilianlabs.org and connect your Metamask wallet.
[Screenshot]()

Press the "admin panel" button in the upper right. This is the list of all polls under your management. Press "Create poll".
[Screenshot]()

Fill in the fields for the poll settings. Pick allowed commodities, minimum contribution, poll starting date and duration and the redistribution algorithm. You can also whitelist specific accounts to have the right to participate in the vote. Press "Start poll".
[Screenshot]()

The ceremony will start accepting contributions until the start of the poll. If a whitelist is specified, only allowed accounts will be able to make contributions. To contribute, open the poll page, pick commodity and amount. You can also provide a description of your Mission to attract more votes in the poll. After filling the form, press "Contribute". You will be asked to sign a transfer transaction in Metamask.
[Screenshot]()

When the the poll starts, all contributors will be able to cast votes for the duration of the poll. To cast a vote, open the poll page, specify the share for each of the missions and press "Vote". Give some shares to the arcoiris team to fund development.
[Screenshot]()

After the poll is complete, either you or any of the voters will be able to complete the ceremony and redistribute funds according to the algorithm. To call for redistribution, open the poll page and press "Distribute".
[Screenshot]()

See [User Guides](./user_guides.md) to learn how to create custom ceremony settings from scratch.
