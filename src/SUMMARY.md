# Summary

- [Getting Started](./getting_started.md)
- [Tutorial](./tutorial.md)
- [User Guides](./user_guides.md)
  - [Custom redistribution algorithm](./01_custom_redistribution_algorithm.md)
  - [Custom master of ceremonies](./02_custom_master_of_ceremonies.md)
- [Design](./design.md)
- [Requirements](./requirements.md)
