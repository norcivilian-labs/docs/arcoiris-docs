# Custom master of ceremonies

A voting contract is the recommended method to manage redistribution ceremonies. You can also specify an arbitrary address as the master of ceremonies. But you can also implement custom logic for deciding the shares of redistribution. This example will distribute shares according to each voter's rating in a on-chain quiz.

The culmination of each MC contract is a call to `arcoiris.redistribute(uint256 gatheringID, uint256 ceremonyID, address[] calldata siblings, uint256[] calldata priorities)`. The `priority` of each `sibling` will decide the share of wealth they receive. For the quiz contract, each `priority` should equal the amount of points won in the quiz.
```solidity
        address[] memory siblings = arcoiris.getContributors(
            quizzes[quizID].gatheringID,
            quizzes[quizID].ceremonyID
        );

        uint256[] memory priorities = new uint256[](siblings.length);

        for (uint256 i = 0; i < siblings.length; i++) {
            priorities[i] = quizzes[quizID].points[siblings[i]];
        }
```

The amount of points for each player in `quizzes[quizID].points[siblingAddress]` is decided by comparing player's guesses to correct answers. Both the guesses and the answers are stored on-chain according to a commit-reveal scheme. The quiz moderator commits hashes of correct answers, players commit hashes of guesses, then everyone reveals the original strings.
```solidity
for (uint256 i = 0; i < quizzes[quizID].players.length; i++) {
            address player = quizzes[quizID].players[i];

            for (uint256 j = 0; j < quizzes[quizID].hashes[address(this)].length; j++) {

                bytes32 hash = keccak256(quizzes[quizID].guesses[player][j]);
                
                bytes32 hashCorrect = quizzes[quizID].hashes[address(this)][j];

                if (hash == hashCorrect) {
                    quizzes[quizID].points[player]++;
                }
            }
        }
```

See the [source code](https://gitlab.com/norcivilian-labs/acroiris/blob/main/contracts/mcs/QuizMC.sol) for this contract.
