# Requirements

- user must contribute wealth
- user must withdraw wealth
- user must vote in a poll
- user must receive a share of wealth
- user must contribute multiple various wealth commodities
- developer must create a gathering
- master of ceremonies must create a ceremony
- master of ceremonies must redistribute wealth on vote
- master of ceremonies must redistribute wealth randomly
- master of ceremonies must redistribute wealth arbitrarily
- poller must start vote
- poller must end vote
- poller must redistribute wealth
- voter must vote anonymously
- developer must add redistribution algorithm
- developer must add automated master of ceremonies

## technical
- user can contribute ERC1155
- user can contribute ERC20
- user can contribute multiple various ERC20
- admin can specify conversin rates for multi-token collection
