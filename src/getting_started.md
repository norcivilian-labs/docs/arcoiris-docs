# Getting Started

To take part in a redistribution ceremony, open the application page at https://arcoiris.norcivilianlabs.org and connect your Metamask wallet.

Choose from the list of polls, make your contribution and cast your vote in the poll. When the voting completes, you will receive your share of the contribution pool.

See [Tutorial](./tutorial.md) on how to start a new voting ceremony, and [User Guides](./user_guides.md) on creating custom ceremonies from scratch.
