# Design

wealth redistribution protocol

competes: kickstarter, patreon, gofundme, gnosis safe

interacts: blockchain nodes, externally owned blockchain accounts

constitutes: decentralized application

includes: gathering factory, gathering store, voting mc contract, redistribution algorithm contracts

resembles: gnosis safe, snapshot

patterns: MVC, factory

stakeholders: fetsorn
